//
//  ImageProviderResults.swift
//  Stocks Photo Viewer
//
//  Created by Vasilii Kasnitski on 10/25/18.
//  Copyright © 2018 Vasilii Kasnitski. All rights reserved.
//

import UIKit

struct ImageProviderResults {
    
    let searchTerm : String
    var photos : [GalleryPhoto]
    var collapsed: Bool = false
}
