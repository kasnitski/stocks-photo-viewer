//
//  ImageDownloadWorker.swift
//  Stocks Photo Viewer
//
//  Created by Vasilii Kasnitski on 10/26/18.
//  Copyright © 2018 Vasilii Kasnitski. All rights reserved.
//

import UIKit

typealias ImageDownloadHandler = (_ image: UIImage?, _ url: URL, _ indexPath: IndexPath?, _ error: Error?) -> Void

class ImageDownloadWorker: NSObject {
    
    // MARK: - Initialization
    
    static let shared = ImageDownloadWorker()
    
    
    // MARK: - Properties
    
    private var completionHandler: ImageDownloadHandler?
    
    lazy var imageDownloadQueue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "com.imageDownloadQueue"
        queue.qualityOfService = .userInteractive
        return queue
    }()
    
    
    // MARK: - Public methods
    
    func downloadImage(_ photo: GalleryPhoto,
                       indexPath: IndexPath?,
                       size: PhotoSize = PhotoSize.thumbnail,
                       handler: @escaping ImageDownloadHandler) {
        
        self.completionHandler = handler
        
        // Define url by necessary image size
        let url = photo.urlForSize(size)
        
        // Check whether exist download task that downloads now this image
        if let operations = (imageDownloadQueue.operations as? [ImageDownloadOperation])?.filter(
            {
                $0.imageUrl.absoluteString == url.absoluteString &&
                $0.isFinished == false &&
                $0.isExecuting == true }
            ),
            let operation = operations.first {
            
            print("Increase the priority for \(url)")
            operation.queuePriority = .veryHigh
        }
        else {
            // Create new task to image downloading
            print("Create a new task for \(url)")
            let operation = ImageDownloadOperation(url: url, indexPath: indexPath)
            if indexPath == nil {
                operation.queuePriority = .high
            }
            operation.downloadHandler = { (image, url, indexPath, error) in
                self.completionHandler?(image, url, indexPath, error)
            }
            imageDownloadQueue.addOperation(operation)
        }
    }
    
    func slowDownImageDownloadTask(_ photo: GalleryPhoto,
                                   size: PhotoSize = PhotoSize.thumbnail) {
        let url = photo.urlForSize(size)
        
        if let operations = (imageDownloadQueue.operations as? [ImageDownloadOperation])?.filter({$0.imageUrl.absoluteString == url.absoluteString && $0.isFinished == false && $0.isExecuting == true }), let operation = operations.first {
            
            print("Reduce the priority for \(url)")
            operation.queuePriority = .low
        }
    }
    
    func cancelAll() {
        imageDownloadQueue.cancelAllOperations()
    }
}
