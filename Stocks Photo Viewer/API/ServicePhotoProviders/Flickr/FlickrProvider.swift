//
//  FlickrProvider.swift
//  Stocks Photo Viewer
//
//  Created by Vasilii Kasnitski on 10/25/18.
//  Copyright © 2018 Vasilii Kasnitski. All rights reserved.
//

import UIKit

class FlickrProvider: ServicePhotoProviderProtocol {

    // MARK: Properties
    
    private let processingQueue = OperationQueue()
    
    
    // MARK: Public methods
    
    func searchPhotosForTerm(_ searchTerm: String, page: Int, completion: @escaping PhotoServiceCompletion) {
        
        guard let searchURL = FlickrHelper.flickrSearchURLForSearchTerm(searchTerm, page: page) else {
            let APIError = NSError(domain: "FlickrSearch", code: 0, userInfo: [NSLocalizedFailureReasonErrorKey:"Unknown API response"])
            completion(nil, APIError)
            return
        }
        
        processFlickrResponseForURL(searchURL, searchTerm: searchTerm, completion: completion)
    }
    
    func popularPhotos(page: Int, completion: @escaping PhotoServiceCompletion) {
        
        guard let popularURL = FlickrHelper.flickrPopular(page: page) else {
            let APIError = NSError(domain: "FlickrPopular", code: 0, userInfo: [NSLocalizedFailureReasonErrorKey:"Unknown API response"])
            completion(nil, APIError)
            return
        }
        
        processFlickrResponseForURL(popularURL, completion: completion)
    }
    
    
    // MARK: Private methods
    
    private func processFlickrResponseForURL(_ url: URL,
                                             searchTerm: String = "",
                                             completion: @escaping PhotoServiceCompletion) {
        
        let searchRequest = URLRequest(url: url)
        
        URLSession.shared.dataTask(with: searchRequest, completionHandler: { (data, response, error) in
            
            if let _ = error {
                let APIError = NSError(domain: "FlickrSearch", code: 0, userInfo: [NSLocalizedFailureReasonErrorKey:"Unknown API response"])
                OperationQueue.main.addOperation({
                    completion(nil, APIError)
                })
                return
            }
            
            guard let _ = response as? HTTPURLResponse,
                let data = data else {
                    let APIError = NSError(domain: "FlickrSearch", code: 0, userInfo: [NSLocalizedFailureReasonErrorKey:"Unknown API response"])
                    OperationQueue.main.addOperation({
                        completion(nil, APIError)
                    })
                    return
            }
            
            do {
                guard let resultsDictionary = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions(rawValue: 0)) as? [String: AnyObject],
                    let stat = resultsDictionary["stat"] as? String else {
                        
                        let APIError = NSError(domain: "FlickrSearch", code: 0, userInfo: [NSLocalizedFailureReasonErrorKey:"Unknown API response"])
                        OperationQueue.main.addOperation({
                            completion(nil, APIError)
                        })
                        return
                }
                
                switch (stat) {
                case "ok":
                    print("Results processed OK")
                case "fail":
                    if let message = resultsDictionary["message"] {
                        
                        let APIError = NSError(domain: "FlickrSearch", code: 0, userInfo: [NSLocalizedFailureReasonErrorKey:message])
                        
                        OperationQueue.main.addOperation({
                            completion(nil, APIError)
                        })
                    }
                    
                    let APIError = NSError(domain: "FlickrSearch", code: 0, userInfo: nil)
                    
                    OperationQueue.main.addOperation({
                        completion(nil, APIError)
                    })
                    
                    return
                default:
                    let APIError = NSError(domain: "FlickrSearch", code: 0, userInfo: [NSLocalizedFailureReasonErrorKey:"Unknown API response"])
                    OperationQueue.main.addOperation({
                        completion(nil, APIError)
                    })
                    return
                }
                
                guard let photosContainer = resultsDictionary["photos"] as? [String: AnyObject], let photosReceived = photosContainer["photo"] as? [[String: AnyObject]] else {
                    
                    let APIError = NSError(domain: "FlickrSearch", code: 0, userInfo: [NSLocalizedFailureReasonErrorKey:"Unknown API response"])
                    OperationQueue.main.addOperation({
                        completion(nil, APIError)
                    })
                    return
                }
                
                var flickrPhotos = [GalleryPhoto]()
                
                for photoObject in photosReceived {
                    guard let photoID = photoObject["id"] as? String,
                        let farm = photoObject["farm"] as? Int ,
                        let server = photoObject["server"] as? String ,
                        let secret = photoObject["secret"] as? String else {
                            break
                    }
                    
                    let thumbnailURL = FlickrHelper.flickrImageURL(photoID: photoID, farm: farm, server: server, secret: secret)
                    let largeImageURL = FlickrHelper.flickrImageURL(PhotoSize.large, photoID: photoID, farm: farm, server: server, secret: secret)
                    
                    guard (thumbnailURL != nil), (largeImageURL != nil) else {
                        break
                    }
                    
                    let photo = GalleryPhoto(thumbnailURL: thumbnailURL!,
                                             largeImageURL: largeImageURL!)
                    flickrPhotos.append(photo)
                }
                
                OperationQueue.main.addOperation({
                    completion(ImageProviderResults(searchTerm: searchTerm, photos: flickrPhotos, collapsed: false), nil)
                })
                
            } catch _ {
                completion(nil, nil)
                return
            }
            
            
        }) .resume()
    }
}
