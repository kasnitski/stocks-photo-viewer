//
//  FlickrHelper.swift
//  Stocks Photo Viewer
//
//  Created by Vasilii Kasnitski on 10/26/18.
//  Copyright © 2018 Vasilii Kasnitski. All rights reserved.
//

import UIKit

let apiKey = "72e8967d89067fd933984bcfadbf79e1"

enum PhotoSize: String {
    case thumbnail = "t"
//    case small = "m"
    case large = "b"
}

enum PopularPeople: String {
    case KennyBarker = "37708438@N05"
}

class FlickrHelper: NSObject {
    
    // MARK: - Public methods
    
    class func flickrImageURL(_ size: PhotoSize = PhotoSize.thumbnail,
                              photoID: String,
                              farm: Int,
                              server: String,
                              secret: String) -> URL? {
        
        if let url =  URL(string: "https://farm\(farm).staticflickr.com/\(server)/\(photoID)_\(secret)_\(size.rawValue).jpg") {
            return url
        }
        return nil
    }
    
    class func flickrSearchURLForSearchTerm(_ searchTerm: String, page: Int) -> URL? {
        
        guard let escapedTerm = searchTerm.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics) else {
            return nil
        }
        
        let URLString = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(apiKey)&text=\(escapedTerm)&per_page=20&page=\(page)&format=json&nojsoncallback=1"
        
        guard let url = URL(string: URLString) else {
            return nil
        }
        
        return url
    }
    
    class func flickrPopular(page: Int = 1, userId: PopularPeople = PopularPeople.KennyBarker) -> URL? {
        
        let URLString = "https://api.flickr.com/services/rest/?method=flickr.photos.getPopular&api_key=\(apiKey)&user_id=\(userId.rawValue)&per_page=20&page=\(page)&format=json&nojsoncallback=1"
        
        guard let url = URL(string: URLString) else {
            return nil
        }
        
        return url
    }
}
