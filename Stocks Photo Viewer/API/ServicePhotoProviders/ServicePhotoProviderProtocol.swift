//
//  ServicePhotoProviderProtocol.swift
//  Stocks Photo Viewer
//
//  Created by Vasilii Kasnitski on 11/8/18.
//  Copyright © 2018 Vasilii Kasnitski. All rights reserved.
//

import UIKit

typealias PhotoServiceCompletion = (_ results: ImageProviderResults?, _ error : NSError?) -> Void

protocol ServicePhotoProviderProtocol {

    func searchPhotosForTerm(_ searchTerm: String, page: Int, completion: @escaping PhotoServiceCompletion)
    func popularPhotos(page: Int, completion: @escaping PhotoServiceCompletion)
}
