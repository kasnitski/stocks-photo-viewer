//
//  GalleryPhoto.swift
//  Stocks Photo Viewer
//
//  Created by Vasilii Kasnitski on 10/25/18.
//  Copyright © 2018 Vasilii Kasnitski. All rights reserved.
//

import UIKit

class GalleryPhoto : Equatable {
    let thumbnailURL: URL
    let largeImageURL: URL
    
    var isLargeAvailable: Bool
    var id: Int = 0
    var thumbnail : UIImage?
    var date: Date?
    
    init (thumbnailURL: URL, largeImageURL: URL) {
        self.thumbnailURL = thumbnailURL
        self.largeImageURL = largeImageURL
        self.isLargeAvailable = Utils.imageExist(fileName: largeImageURL.lastPathComponent)
        self.date = Date()
    }
    
    func urlForSize(_ size: PhotoSize) -> URL {
        switch size {
        case .thumbnail:
            return thumbnailURL
        case .large:
            return largeImageURL
        }
    }
}

func == (lhs: GalleryPhoto, rhs: GalleryPhoto) -> Bool {
    return lhs.thumbnailURL == rhs.thumbnailURL
}
