//
//  GalleryRouter.swift
//  Stocks Photo Viewer
//
//  Created by Vasilii Kasnitski on 10/25/18.
//  Copyright (c) 2018 Vasilii Kasnitski. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol GalleryRoutingLogic {
    func routeToFullScreenViewController(openedPhoto: GalleryPhoto, selectedImageView: UIImageView)
}

protocol GalleryDataPassing {
    var dataStore: GalleryDataStore? { get }
}

class GalleryRouter: NSObject, GalleryRoutingLogic, GalleryDataPassing {
    
    // MARK: - Properties
    
    weak var viewController: GalleryViewController?
    internal var dataStore: GalleryDataStore?
    fileprivate let transition = OpenPhotoAnimator()
    fileprivate var selectedImageView: UIImageView?
    
    
    // MARK: - Routing
    
    func routeToFullScreenViewController(openedPhoto: GalleryPhoto, selectedImageView: UIImageView) {
        self.selectedImageView = selectedImageView
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destinationVC = storyboard.instantiateViewController(withIdentifier: "FullScreenViewController") as! FullScreenViewController
        var destinationDS = destinationVC.router!.dataStore!
        passDataToFullScreen(openedPhoto: openedPhoto, source: dataStore!, destination: &destinationDS)
        navigateToSomewhere(source: viewController!, destination: destinationVC)
    }
    
    
    // MARK: - Navigation
    
    func navigateToSomewhere(source: GalleryViewController, destination: FullScreenViewController) {
        transition.dismissCompletion = { [weak self] in
            guard let self = self else { return }
            self.selectedImageView!.isHidden = false
        }
        
        destination.transitioningDelegate = self
        source.present(destination, animated: true, completion: nil)
    }
    
    
    // MARK: - Passing data
    
    func passDataToFullScreen(openedPhoto: GalleryPhoto,
                              source: GalleryDataStore,
                              destination: inout FullScreenDataStore) {
        
        destination.openedPhoto = openedPhoto
    }
}


// MARK: - Transition delegate

extension GalleryRouter: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        transition.originFrame = selectedImageView!.superview!.convert(selectedImageView!.frame, to: nil)
        transition.presenting = true
        selectedImageView!.isHidden = true
        
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? { 
        transition.presenting = false
        return transition
    }
}
