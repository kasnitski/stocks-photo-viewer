//
//  GallerySectionHeader.swift
//  Stocks Photo Viewer
//
//  Created by Vasilii Kasnitski on 11/4/18.
//  Copyright © 2018 Vasilii Kasnitski. All rights reserved.
//

import UIKit


protocol GallerySectionHeaderDelegate {
    
    func deleteSection(_ section: Int)
    func collapseSection(_ section: Int)
    func openSection(_ section: Int)
}

enum GallerySectionState {
    case collapsed
    case opened
}


class GallerySectionHeader: UICollectionReusableView {

    // MARK: - Properties
    
    public var delegate: GallerySectionHeaderDelegate?
    private var section: Int = 0
    private var state: GallerySectionState = .opened
    private var name: String = ""
    private var isDeleteConfirmationState = false
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var collapseButton: UIButton!
    @IBOutlet weak var labelName: UILabel!
    
    
    // MARK: - Initialization
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    private struct Const {
        static let imageCollapsed = UIImage(named: "collapse")
        static let imageDelete = UIImage(named: "delete")
        static let imageCancel = UIImage(named: "cancel")
        static let imageAccept = UIImage(named: "accept")
    }
    
    
    // MARK: - Actions
    
    @IBAction func clickOnCloseButton(_ sender: Any) {
        
        // Initial resources for current state
        let rightButtonImage = isDeleteConfirmationState ? Const.imageCancel : Const.imageDelete
        let rightButtonImage2 = isDeleteConfirmationState ? Const.imageDelete : Const.imageCancel
        let leftButtonImage = isDeleteConfirmationState ? Const.imageCollapsed : Const.imageAccept
        let centerText = isDeleteConfirmationState ? self.name : "Are you sure?"
        let angle = isDeleteConfirmationState ? CGFloat.pi / 2 : CGFloat.pi
        let duration = isDeleteConfirmationState ? 0.4 : 0.5
        
        // Create fake button
        let fakeButton = UIButton(frame: deleteButton.frame)
        fakeButton.setBackgroundImage(rightButtonImage, for: .normal)
        self.addSubview(fakeButton)
        
        // Make main (delete) button invisible
        self.deleteButton.isHidden = true
        self.collapseButton.layer.zPosition = 2
        
        // Create destination position for fake button
        var destinationFrame = self.deleteButton.frame
        destinationFrame.origin.x = self.collapseButton.frame.size.width
        
        UIView.animate(withDuration: duration, animations: {
            
            // Animate fake button frame and rotate it
            fakeButton.frame = destinationFrame
            fakeButton.transform = CGAffineTransform(rotationAngle: angle)
            
            // Make collapse button and center label invisible
            self.collapseButton.alpha = 0.0
            self.labelName.alpha = 0.0
            
        }) { (completed) in
            
            // Replace image for left button
            self.collapseButton.setBackgroundImage(leftButtonImage, for: .normal)
            
            // Nullify transform state of fake button
            fakeButton.transform = CGAffineTransform.identity
            
            UIView.animate(withDuration: duration, animations: {
                
                // Make collapse button and center label visible and change text
                self.collapseButton.alpha = 1.0
                self.labelName.alpha = 1.0
                self.labelName.text = centerText
                
                // Animate fake button frame and rotate it back
                fakeButton.frame = self.deleteButton.frame
                fakeButton.transform = CGAffineTransform(rotationAngle: angle)
                
            }, completion: { (completed) in
                
                // Make main button visible again
                self.deleteButton.isHidden = false
                self.deleteButton.setBackgroundImage(rightButtonImage2, for: .normal)
                
                // Remove fake button from the view
                fakeButton.removeFromSuperview()
                
                // Update current state
                self.isDeleteConfirmationState = !self.isDeleteConfirmationState
            })
        }
    }
    
    @IBAction func clickOnCollapseButton(_ sender: Any) {
        guard let delegate = delegate else { return }
        if isDeleteConfirmationState {
            delegate.deleteSection(section)
        }
        else {
            switch state {
            case .opened:
                delegate.collapseSection(section)
                state = .collapsed
                break
            case .collapsed:
                delegate.openSection(section)
                state = .opened
                break
            }
        }
    }
    
    
    // MARK: - Public methods
    
    func updateName(_ name: String, section: Int, state: GallerySectionState = .opened) {
        self.state = state
        self.section = section
        self.name = name
        labelName.text = name
    }
    
    func restoreDefaultState() {
        isDeleteConfirmationState = false
        self.collapseButton.setBackgroundImage(Const.imageCollapsed, for: .normal)
        self.deleteButton.setBackgroundImage(Const.imageDelete, for: .normal)
        self.labelName.text = self.name
    }
}
