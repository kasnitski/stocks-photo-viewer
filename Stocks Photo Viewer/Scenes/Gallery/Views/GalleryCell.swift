//
//  GalleryCell.swift
//  Stocks Photo Viewer
//
//  Created by Vasilii Kasnitski on 10/25/18.
//  Copyright © 2018 Vasilii Kasnitski. All rights reserved.
//

import UIKit

class GalleryCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var viewLargePhotoStatus: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageView.layer.borderColor = UIColor.lightGray.cgColor
        imageView.layer.borderWidth = 0.5
        
        let radius = viewLargePhotoStatus.frame.size.height / 2.0
        viewLargePhotoStatus.layer.cornerRadius = radius
    }
    
    func updateWithPhoto(_ photo: GalleryPhoto) {
        imageView.image = photo.thumbnail
        viewLargePhotoStatus.backgroundColor = photo.isLargeAvailable ? UIColor.green : UIColor.red
    }
}
