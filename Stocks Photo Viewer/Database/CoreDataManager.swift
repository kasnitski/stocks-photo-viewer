//
//  DatabaseManager.swift
//  Stocks Photo Viewer
//
//  Created by Vasilii Kasnitski on 11/1/18.
//  Copyright © 2018 Vasilii Kasnitski. All rights reserved.
//

import UIKit
import CoreData

class DatabaseManager: NSObject {

    // MARK: Initialization
    
    public static let shared = DatabaseManager()
    private override init() {}
    
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Stocks_Photo_Viewer")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func managedObjectContext() -> NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    // MARK: Public methods
    
    func entityForName(entityName: String) -> NSEntityDescription {
        return NSEntityDescription.entity(forEntityName: entityName, in: managedObjectContext())!
    }
    
    func savePhotos(result: ImageProviderResults) {
        var searchRequest = findSearchRequest(term: result.searchTerm)
        if searchRequest == nil {
            searchRequest = MSearchRequest()
            searchRequest!.searchTerm = result.searchTerm
            searchRequest!.date = NSDate()
        }
        
        var mPhotos = [MGalleryPhoto]()
        var i = searchRequest?.photos?.count ?? 0
        for photo in result.photos {
            let mPhoto = MGalleryPhoto()
            mPhoto.thumbnailURL = photo.thumbnailURL.absoluteString
            mPhoto.largeImageURL = photo.largeImageURL.absoluteString
            mPhoto.id = i
            i += 1
            mPhotos.append(mPhoto)
        }
        
        searchRequest!.addToPhotos(NSSet(array: mPhotos))
        
        saveContext()
    }
    
    func deleteImageResultsBySearchTerm(_ searchTerm: String) {
        guard let result = findSearchRequest(term: searchTerm) else { return }
        
        managedObjectContext().delete(result)
        saveContext()
    }
    
    func findSearchRequest(term: String) -> MSearchRequest? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MSearchRequest")
        fetchRequest.predicate = NSPredicate(format: "searchTerm == %@", term)
        
        do {
            let data = try DatabaseManager.shared.managedObjectContext().fetch(fetchRequest)
            return data.first as? MSearchRequest
        } catch {
            print(error)
        }
        
        return nil
    }
    
    func savedResults() -> [ImageProviderResults] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MSearchRequest")
        do {
            var results = [ImageProviderResults]()
            let data = try DatabaseManager.shared.managedObjectContext().fetch(fetchRequest)
            
            for oneElement in data as! [MSearchRequest] {
                
                var photos = [GalleryPhoto]()
                
                oneElement.photos?.forEach({ (any) in
                    let mPhoto = any as! MGalleryPhoto
                    
                    if let thumbnailURL = mPhoto.thumbnailURL,
                        let largeImageURL = mPhoto.largeImageURL {
                        let galleryPhoto = GalleryPhoto(thumbnailURL: URL(string: thumbnailURL)!,
                                                        largeImageURL: URL(string: largeImageURL)!)
                        galleryPhoto.id = mPhoto.id
                        photos.append(galleryPhoto)
                    }
                })
                
                photos.sort(by: {
                    return $0.id < $1.id
                })
                
                let result = ImageProviderResults(searchTerm: oneElement.searchTerm ?? "", photos: photos, collapsed: false)
                results.append(result)
            }
            
            return results
        } catch {
            print(error)
        }
        
        return []
    }
}
