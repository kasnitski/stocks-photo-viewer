//
//  MSearchRequest+CoreDataProperties.swift
//  Stocks Photo Viewer
//
//  Created by Vasilii Kasnitski on 11/2/18.
//  Copyright © 2018 Vasilii Kasnitski. All rights reserved.
//
//

import Foundation
import CoreData


extension MSearchRequest {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MSearchRequest> {
        return NSFetchRequest<MSearchRequest>(entityName: "MSearchRequest")
    }

    @NSManaged public var searchTerm: String?
    @NSManaged public var date: NSDate?
    @NSManaged public var photos: NSSet?

}

// MARK: Generated accessors for photos
extension MSearchRequest {

    @objc(addPhotosObject:)
    @NSManaged public func addToPhotos(_ value: MGalleryPhoto)

    @objc(removePhotosObject:)
    @NSManaged public func removeFromPhotos(_ value: MGalleryPhoto)

    @objc(addPhotos:)
    @NSManaged public func addToPhotos(_ values: NSSet)

    @objc(removePhotos:)
    @NSManaged public func removeFromPhotos(_ values: NSSet)

}
