//
//  MSearchRequest+CoreDataClass.swift
//  Stocks Photo Viewer
//
//  Created by Vasilii Kasnitski on 11/2/18.
//  Copyright © 2018 Vasilii Kasnitski. All rights reserved.
//
//

import Foundation
import CoreData

@objc(MSearchRequest)
public class MSearchRequest: NSManagedObject {
    convenience init() {
        self.init(entity: DatabaseManager.shared.entityForName(entityName: "MSearchRequest"),
                  insertInto: DatabaseManager.shared.managedObjectContext())
    }
}
