//
//  MGalleryPhoto+CoreDataProperties.swift
//  Stocks Photo Viewer
//
//  Created by Vasilii Kasnitski on 11/2/18.
//  Copyright © 2018 Vasilii Kasnitski. All rights reserved.
//
//

import Foundation
import CoreData


extension MGalleryPhoto {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MGalleryPhoto> {
        return NSFetchRequest<MGalleryPhoto>(entityName: "MGalleryPhoto")
    }

    @NSManaged public var largeImageURL: String?
    @NSManaged public var thumbnailURL: String?
    @NSManaged public var id: Int
    @NSManaged public var searchRequest: MSearchRequest?

}
