//
//  PhotoProvider.swift
//  Stocks Photo Viewer
//
//  Created by Vasilii Kasnitski on 10/31/18.
//  Copyright © 2018 Vasilii Kasnitski. All rights reserved.
//

import UIKit

class PhotoProvider: NSObject {
    
    // MARK: - Initialization
    
    public static let shared = PhotoProvider()
    
    
    // MARK: - Properties
    
    let imageCache = NSCache<NSString, UIImage>()
    
    
    // MARK: Public methods
    
    func getPhoto(_ photo: GalleryPhoto,
                  indexPath: IndexPath?,
                  size: PhotoSize = PhotoSize.thumbnail,
                  completion: @escaping ImageDownloadHandler) {
        
        let requestedURL = photo.urlForSize(size)
        let fileName = requestedURL.lastPathComponent
        
        if let cachedImage = imageCache.object(forKey: fileName as NSString) {
            // Check for the cached image first
            print("Return cached Image for \(fileName)")
            completion(cachedImage, requestedURL, indexPath, nil)
        }
        else {
            let image: UIImage? = Utils.loadImage(fileName: fileName)
            if image != nil {
                print("Return saved Image for \(fileName)")
                savePhotoToCache(photo: image, key: fileName)
                completion(image!, requestedURL, indexPath, nil)
            }
            else {
                ImageDownloadWorker.shared.downloadImage(photo, indexPath: indexPath, size: size) { [weak self] (image, url, indexPath, error) in
                    
                    guard let self = self else { return }
                    guard let image = image else { return }
                    
                    // Save locally
                    Utils.save(image: image, fileName: url.lastPathComponent)
                    
                    // Save to cache
                    self.savePhotoToCache(photo: image, key: fileName)
                    
                    // Handle completion
                    completion(image, url, indexPath, nil)
                }
            }
        }
    }
    
    func deletePhoto(_ photo: GalleryPhoto) {
        let thumbnailName = photo.thumbnailURL.lastPathComponent
        let largePhotoName = photo.largeImageURL.lastPathComponent
        
        // Delete from cached directory
        Utils.deleteCachedImage(fileName: thumbnailName)
        Utils.deleteCachedImage(fileName: largePhotoName)
        
        // Delete from cache
        imageCache.removeObject(forKey: thumbnailName as NSString)
        imageCache.removeObject(forKey: largePhotoName as NSString)
    }
    
    
    // MARK: - Private methods
    
    func savePhotoToCache(photo: UIImage?, key: String) {
        if let newPhoto = photo {
            imageCache.setObject(newPhoto, forKey: key as NSString)
        }
    }
}
