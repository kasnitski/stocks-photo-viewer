//
//  Utils.swift
//  Stocks Photo Viewer
//
//  Created by Vasilii Kasnitski on 10/31/18.
//  Copyright © 2018 Vasilii Kasnitski. All rights reserved.
//

import UIKit

class Utils: NSObject {
    
    // MARK: - Initialization
    
    private struct Const {
        static let formatter: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            return formatter
        }()
        
        static let cacheFolderName = "ImagesCache/"
    }
    
    
    // MARK: - Public methods
    
    class func createCacheFolder() {
        let exist = directoryExistsAtPath(cacheFolderUrl.relativePath)
        if exist { return }
        
        do {
            try FileManager.default.createDirectory(atPath: cacheFolderUrl.relativePath,
                                                    withIntermediateDirectories: true,
                                                    attributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription);
        }
    }
    
    class func dateString(date: Date) -> String {
        return Const.formatter.string(from: date)
    }
    
    class func save(image: UIImage, fileName: String) {
        let fileURL = cacheFolderUrl.appendingPathComponent(fileName)
        if let imageData = image.jpegData(compressionQuality: 1.0) {
            do {
                print("Save image to file \(fileURL)")
                try imageData.write(to: fileURL)
            } catch let error as NSError {
                print(error.localizedDescription);
            }
        }
    }
    
    class func deleteCachedImage(fileName: String) {
        let fileURL = cacheFolderUrl.appendingPathComponent(fileName)
        if FileManager.default.fileExists(atPath: fileURL.relativePath) {
            do {
                try FileManager.default.removeItem(at: fileURL)
            } catch let error as NSError {
                print(error.localizedDescription);
            }
        }
    }
    
    class func imageExist(fileName: String) -> Bool {
        let fileURL = cacheFolderUrl.appendingPathComponent(fileName)
        return FileManager.default.fileExists(atPath: fileURL.relativePath)
    }
    
    class func loadImage(fileName: String) -> UIImage? {
        let fileURL = cacheFolderUrl.appendingPathComponent(fileName)
        
        do {
            let imageData = try Data(contentsOf: fileURL)
            return UIImage(data: imageData)
        }
        catch let error as NSError {
            print("Error during loading image : \(error)")
        }
        return nil
    }
    
    class func randomBool() -> Bool {
        return arc4random_uniform(2) == 0
    }
    
    class func randomInt(max: Int) -> Int {
        return Int(arc4random_uniform(UInt32(max)))
    }
    
    
    // MARK: - Private methods
    
    class private var cacheFolderUrl: URL {
        let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let url = documentsUrl.appendingPathComponent(Const.cacheFolderName)
        return url
    }
    
    private class func directoryExistsAtPath(_ path: String) -> Bool {
        var isDirectory = ObjCBool(true)
        let exists = FileManager.default.fileExists(atPath: path, isDirectory: &isDirectory)
        return exists && isDirectory.boolValue
    }
}
