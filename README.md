# Stocks Photo Viewer

    This is the photo viewer app which fetches photos from a 3rd party photo-sharing services. On the first stage it fetches photos only from the Flickr. 


1. Architecture: Clean Swift (for more information: https://clean-swift.com)

2. Main functionality:
    - 2 screen: Gallery and Full Screen Scenes
    - fetching photos from one popular user from the Flickr
    - infinite bulk photos loading (works for popular section or for the last search section)
    - smooth infinite photos loading implemented
    - async photos loading
    - search photos
    - every new search result adds to the new section. 
    - ability to collapse/open section
    - ability to delete section
    - animation for the delete action added
    - left swipe on the Full Screen Scene to close scene implemented
    - cache for photos implemented (3 levels: cache -> documents directory -> download)
    - save to / remove from documents directory implemented
    - database layer implemented, we store last photos results
    - progress bar (3rd party) added to the Full Screen Scene
    - performance optimization (updates with throttling)


3. What I would like to improve:
    - add more services to fetch the photos (500px, dribbble, etc.)
    - custom layout to display main screen in more beautiful way
    - add pan gestures to close Full Screen Scene
    - add pintch gestures to scale photo on Full Screen Scene
    - implement bulk loading during loading photos from database
    - add ability to share photo to external services / save to camera roll
    - add tests to the main functionality (all logic parts are testable with current architecture)


How to run:
   Just download and click 'Run' button in XCode.
   
